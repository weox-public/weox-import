import pendulum
import time_machine
from decimal import Decimal

from weox_import import Pricelist, Product, Specification, locales


def test_no_products():

    pl = Pricelist("test", "1.0")

    result, error = pl.validate()

    assert result is False


def test_min_product():

    pl = Pricelist("test", "1.0")

    product = Product("1")
    product.add_category("Root")
    product.add_name("Test Product")

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True


def test_product_without_name():
    pl = Pricelist("test", "1.0")

    product = Product("1")
    product.add_category("Root")
    product.add_name("Test Product")
    pl.add_product(product)

    product = Product("2")
    product.add_category("Root")
    pl.add_product(product)

    result, error = pl.validate()

    assert result is True

    assert len(pl.products) == 1


def test_default_category():

    pl = Pricelist("test", "1.0")

    product = Product("1")
    product.add_name("Test Product")

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True

    assert len(pl.products[0].find("categories")) == 1

    assert pl.products[0].find("categories")[0].text == "Unsorted"


def test_default_brand():

    pl = Pricelist("test", "1.0")

    product = Product("1")
    product.add_name("Test Product")

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True

    assert pl.products[0].find("brand").text == "Noname"


@time_machine.travel("2023-01-01 00:00 +0000", tick=False)
def test_max_product():

    pl = Pricelist("test", "1.0")

    product = Product("1")
    product.set_old_id("old-1")
    product.set_brand("Asus")
    product.set_supplier_code("supplier_code")

    product.add_ean("0075678164125")
    product.add_ean("4026908001150")
    product.add_ean("30074576")
    product.add_ean("BESTBEFORE-8595606402416")

    product.add_category("Root")
    product.add_category("Toor")

    product.add_name("Name 1")
    product.add_name("Name 2", locales.LOCALE_RU)
    product.add_name("Название 3", locales.LOCALE_RU)
    product.add_name("Name 4", locales.LOCALE_EN)

    product.add_description("Description 1")
    product.add_description("Description 2", locales.LOCALE_RU)
    product.add_description("Описание 3", locales.LOCALE_RU)
    product.add_description("Description 4", locales.LOCALE_EN)
    product.add_description('<a href="#">link ссылка</a>', locales.LOCALE_RU)

    product.set_quantity(1, "One")
    product.set_stock_date(pendulum.now("UTC").add(days=1).isoformat())

    assert product.quantity == 1
    assert product.quantity_str == "One"
    assert product.stock_date == "2023-01-02"

    product.set_warranty(24, "24 Months")

    product.set_price(Decimal("100.5"), "EUR", vat=0)
    product.set_retail_price(Decimal("200"), "USD")
    product.set_old_retail_price(Decimal("250"), "USD")

    product.add_image("http://example.com/example1.jpg")
    product.add_image("http://example.com/example2.jpg")

    product.set_weox_product_info_url("http://localhost:8888/product_info/1")
    assert product.weox_product_info_url == "http://localhost:8888/product_info/1"

    product.autofill_weox_product_info_url("test-parser", 8888)
    assert product.weox_product_info_url == "http://test-parser:8888/product_info/1"

    sp1 = Specification()
    sp1.add_name("Color")
    sp1.add_value("Red")
    product.add_specification(sp1)

    sp2 = Specification()
    sp2.add_name("Color", locales.LOCALE_EN)
    sp2.add_name("Цвет", locales.LOCALE_RU)
    sp2.add_name("Tsvet")
    sp2.add_value("Red")
    sp2.add_value("Red", locales.LOCALE_EN)
    sp2.add_value("Красный", locales.LOCALE_RU, "Штука")
    product.add_specification(sp2)

    # hash depends on pickle and installed python version
    assert product.hash == "825a1bb01f6c4916bed659c0a57a547a"

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True

    # hash depends on pickle and installed python version
    assert pl.hash.text == "a4d7153cfd5d5a35782d752fcd717c70"


def test_empty_specification():

    pl = Pricelist("test", "1.0")

    sp1 = Specification()

    product = Product("1")
    product.add_name("Test Product")
    product.add_specification(sp1)

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True

    assert pl.products[0].find("brand").text == "Noname"


def test_duplicates():
    pl = Pricelist("test", "1.0")

    p1 = Product("1")
    p1.add_category("Root")
    p1.add_name("Test Product")
    p1.set_sku("SKU")
    p1.add_ean("4006381333931")

    pl.add_product(p1)
    pl.add_product(p1)
