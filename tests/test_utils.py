import pytest
from decimal import Decimal

from weox_import.utils import (
    check_ean,
    compose_url,
    fix_price,
    is_decimal,
    is_empty_string,
    is_numeric,
    is_starts_with_digit,
    str_to_int,
)


def test_check_invalid_ean():
    assert not check_ean("477201309844B")
    assert not check_ean("477201309844")
    assert not check_ean("XXXX")
    assert not check_ean("")


def test_check_valid_ean():
    assert "190198048387" == check_ean("190198048387")
    assert "190198048387" == check_ean("190-19804-8387")
    assert "190198048387" == check_ean("19019 8048387")
    assert "190198048387" == check_ean("190198048387X")
    assert 190198048387 == check_ean(190198048387)


def test_string_is_empty():
    assert is_empty_string(None)
    assert is_empty_string(False)
    assert is_empty_string("")
    assert is_empty_string(" ")
    assert is_empty_string("    ")
    assert is_empty_string("\n")


def test_string_is_not_empty():
    assert not is_empty_string(True)
    assert not is_empty_string(1)
    assert not is_empty_string("a")
    assert not is_empty_string(" ab ")
    assert not is_empty_string("  c  ")


def test_is_numeric():
    assert is_numeric(1)
    assert is_numeric(1111)
    assert is_numeric("1")
    assert is_numeric("1111")
    assert is_numeric(Decimal("123"))

    assert not is_numeric(Decimal("123.12"))
    assert not is_numeric(None)
    assert not is_numeric("")
    assert not is_numeric(False)
    assert not is_numeric(True)
    assert not is_numeric("12a123")
    assert not is_numeric("X12")


def test_is_starts_with_digit():
    assert is_starts_with_digit(0)
    assert is_starts_with_digit(123)
    assert is_starts_with_digit("123")
    assert is_starts_with_digit("1.2,3")
    assert is_starts_with_digit("000")
    assert is_starts_with_digit(Decimal("1"))

    assert not is_starts_with_digit(".,")
    assert not is_starts_with_digit("abc")
    assert not is_starts_with_digit("")


def test_str_to_int():
    assert str_to_int(0) == 0
    assert str_to_int(123) == 123
    assert str_to_int("123") == 123
    assert str_to_int("1.2,3") == 1
    assert str_to_int("000") == 0
    assert str_to_int(Decimal("1")) == 1

    assert str_to_int(".,") == 0
    assert str_to_int("abc") == 0
    assert str_to_int("") == 0


def test_is_decimal():
    assert is_decimal(0)
    assert is_decimal(123)
    assert is_decimal("123")
    assert is_decimal("000")
    assert is_decimal(Decimal("1"))
    assert is_decimal(Decimal("1.5"))
    assert is_decimal("1.5")
    assert is_decimal("1.00")
    assert is_decimal(1.0)
    assert is_decimal(1.5)

    assert not is_decimal("1.")
    assert not is_decimal("1,5")
    assert not is_decimal("1..5")
    assert not is_decimal(".1")
    assert not is_decimal("1.2,3")
    assert not is_decimal(".,")
    assert not is_decimal("abc")
    assert not is_decimal("")


def test_compose_url():
    assert compose_url("localhost", "") == "http://localhost"
    assert compose_url("localhost", "/") == "http://localhost/"
    assert compose_url("localhost", "/test") == "http://localhost/test"


def test_fix_price():
    assert fix_price(Decimal("123.45")) == Decimal("123.45")
    assert fix_price(123.45) == Decimal("123.45")
    assert fix_price("123.45") == Decimal("123.45")
    assert fix_price(123) == Decimal("123")

    assert fix_price(" 123.45 ") == Decimal("123.45")
    assert fix_price(" 123,45 ") == Decimal("123.45")
    assert fix_price(" 1 2 3 , 4 5 ") == Decimal("123.45")

    with pytest.raises(ValueError):
        assert fix_price(None)

    with pytest.raises(ValueError):
        assert fix_price("123e10")

    with pytest.raises(ValueError):
        assert fix_price("123.45.6")
