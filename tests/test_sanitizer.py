from weox_import.sanitizer import sanitize


def test_sanitize():
    html = (
        '<div class="elementor-element elementor-element-0865ddb elementor-widget '
        'elementor-widget-heading" style="font-family: "" data-id="0865ddb" '
        'data-element_type="widget" data-widget_type="heading.default" '
        'data-mce-style="font-family: ";"><div class="elementor-widget-container"><h3 '
        'class="elementor-heading-title elementor-size-default" '
        'data-mce-style="color: var( --e-global-color-primary ); font-size: 27px;" '
        'style="color: var( --e-global-color-primary ); font-size: '
        '27px;">h3</h3><p><img style="font-family: "" '
        'src="https://www.monge.it/en/product/sterilised-monoprotein-duck/" alt="" '
        'data-mce-src="https://www.monge.it/en/product/sterilised-monoprotein-duck/" '
        'data-mce-style="font-family: ";"><span style="font-family: "" '
        'data-mce-style="font-family: ";">p</span></p></div></div><div '
        'class="elementor-element elementor-element-7d5f1e1 elementor-widget '
        'elementor-widget-jet-listing-dynamic-repeater" style="font-family: "" '
        'data-id="7d5f1e1" data-element_type="widget" '
        'data-widget_type="jet-listing-dynamic-repeater.default" '
        'data-mce-style="font-family: ";"><div '
        'class="elementor-widget-container"><div class="jet-listing '
        'jet-listing-dynamic-repeater"><div '
        'class="jet-listing-dynamic-repeater__items "><div '
        'class="jet-listing-dynamic-repeater__item"><img '
        'src="https://www.monge.it/wp-content/uploads/2018/03'
        '/monge_gatto_anatra_appetibilita%CC%80.jpg" alt="" '
        'data-mce-src="https://www.monge.it/wp-content/uploads/2018/03'
        '/monge_gatto_anatra_appetibilita%CC%80.jpg"><img '
        'src="https://www.monge.it/wp-content/uploads/2018/03/monge_gatto_anatra_xos'
        '.jpg" alt="" data-mce-src="https://www.monge.it/wp-content/uploads/2018/03'
        '/monge_gatto_anatra_xos.jpg"><img '
        'src="https://www.monge.it/wp-content/uploads/2018/03'
        '/monge_gatto_anatra_grassi.jpg" alt="" '
        'data-mce-src="https://www.monge.it/wp-content/uploads/2018/03'
        '/monge_gatto_anatra_grassi.jpg"><img '
        'src="https://www.monge.it/wp-content/uploads/2018/01/icona_made_in_italy.jpg'
        '" alt="Made in italy" data-mce-src="https://www.monge.it/wp-content/uploads'
        '/2018/01/icona_made_in_italy.jpg"><img '
        'src="https://www.monge.it/wp-content/uploads/2016/04/unica-proteina-eng.jpg" '
        'alt="" data-mce-src="https://www.monge.it/wp-content/uploads/2016/04/unica'
        '-proteina-eng.jpg"><img src="https://www.monge.it/wp-content/uploads/2018/03'
        '/icona_no_coloranti_e_conservanti_GB.jpg" alt="" '
        'data-mce-src="https://www.monge.it/wp-content/uploads/2018/03'
        '/icona_no_coloranti_e_conservanti_GB.jpg"><img '
        'src="https://www.monge.it/wp-content/uploads/2016/01/icona_no_cruelty_test'
        '.jpg" alt="" data-mce-src="https://www.monge.it/wp-content/uploads/2016/01'
        '/icona_no_cruelty_test.jpg"></div><div '
        'class="jet-listing-dynamic-repeater__item"><br></div><div '
        'class="jet-listing-dynamic-repeater__item"><p><strong>Strong:</strong> '
        "description.<br><strong>Strong 2:</strong> Description 2.<br><strong>Strong "
        "3:</strong> Description 3.<br></p></div></div></div></div></div>"
    )
    expected_sanitized_html = (
        "<h3>h3</h3>"
        "<p>p</p>"
        "<p>"
        "<strong>Strong:</strong> description.<br>"
        "<strong>Strong 2:</strong> Description 2.<br>"
        "<strong>Strong 3:</strong> Description 3.<br>"
        "</p>"
    )

    sanitized = sanitize(html)

    assert sanitized == expected_sanitized_html
    assert sanitize(1.2) == "1.2"
    assert sanitize(None) == ""
    assert sanitize(1) == "1"
    assert sanitize("Test test") == "Test test"
