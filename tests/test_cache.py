import tempfile
import unittest
from io import BufferedReader, BytesIO, StringIO

from weox_import.cache import (
    CACHE_UNLIMITED_TTL,
    Cache,
    FileSystemCacheStorage,
    MemoryCacheStorage,
    TextCacheSerializer,
)


class CacheTestCase(unittest.TestCase):
    def test_cached_json_serializable_memory_storage(self):

        cache = Cache(
            path="/",
            default_ttl=CACHE_UNLIMITED_TTL,
            storage_class=MemoryCacheStorage,
        )

        self.counter = 0

        @cache.cached_json_serializable(filename="testing-request-1.json")
        def test_request_1(foo: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "counter": self.counter,
            }

        @cache.cached_json_serializable(filename="testing-request-2.json")
        def test_request_2(foo: str, bar: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "bar": bar,
                "counter": self.counter,
            }

        assert test_request_1("foo") == {"foo": "foo", "counter": 1}
        assert test_request_1("foo") == {"foo": "foo", "counter": 1}

        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }
        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }

        assert self.counter == 2

    def test_cached_json_serializable_filesystem_storage(self):

        cache_dir = tempfile.gettempdir()

        cache = Cache(
            path=cache_dir,
            default_ttl=CACHE_UNLIMITED_TTL,
            storage_class=FileSystemCacheStorage,
        )

        cache.delete("testing-request-1.json")
        cache.delete("testing-request-2.json")

        self.counter = 0

        @cache.cached_json_serializable(filename="testing-request-1.json")
        def test_request_1(foo: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "counter": self.counter,
            }

        @cache.cached_json_serializable(filename="testing-request-2.json")
        def test_request_2(foo: str, bar: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "bar": bar,
                "counter": self.counter,
            }

        assert test_request_1("foo") == {"foo": "foo", "counter": 1}
        assert test_request_1("foo") == {"foo": "foo", "counter": 1}

        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }
        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }

        assert self.counter == 2

        cache.delete("testing-request-1.json")
        cache.delete("testing-request-2.json")

    def test_cached_json_serializable_filesystem_storage_stream_result(self):
        cache_dir = tempfile.gettempdir()

        cache = Cache(
            path=cache_dir,
            default_ttl=CACHE_UNLIMITED_TTL,
            storage_class=FileSystemCacheStorage,
        )

        cache.delete("testing-request-1.json")

        self.counter = 0

        @cache.cached_json_serializable(
            filename="testing-request-1.json", stream_result=True
        )
        def test_request_1(foo: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "counter": self.counter,
            }

        # Stream result is not supported by JsonSerializer
        assert test_request_1("foo") == {"foo": "foo", "counter": 1}
        assert test_request_1("foo") == {"foo": "foo", "counter": 1}

        assert self.counter == 1

        cache.delete("testing-request-1.json")

    def test_cached_xml_memory_storage(self):
        cache = Cache(
            path="/",
            default_ttl=CACHE_UNLIMITED_TTL,
            storage_class=MemoryCacheStorage,
        )

        self.counter = 0

        assert not cache.exists("testing-request-foo.txt")
        assert not cache.exists("testing-request-foo-bar.txt")

        @cache.cached(filename="testing-request-{}.txt")
        def test_request_1(foo: str):
            self.counter = self.counter + 1

            return f"{foo} {self.counter}"

        @cache.cached(filename="testing-request-{}-{}.txt")
        def test_request_2(foo: str, bar: str):
            self.counter = self.counter + 1

            return f"{foo} {bar} {self.counter}"

        assert test_request_1("foo") == "foo 1"
        assert test_request_1("foo") == "foo 1"

        assert test_request_2("foo", "bar") == "foo bar 2"
        assert test_request_2("foo", "bar") == "foo bar 2"

        assert cache.exists("testing-request-foo.txt")
        assert cache.exists("testing-request-foo-bar.txt")

        assert self.counter == 2

    def test_cached_xml_stream_storage(self):
        cache = Cache(
            path="/",
            default_ttl=CACHE_UNLIMITED_TTL,
            storage_class=MemoryCacheStorage,
            serializer_class=TextCacheSerializer,
        )

        cache_key = "large-file.txt"

        assert not cache.exists(cache_key)

        self.counter = 0

        @cache.cached(filename=cache_key)
        def test_large_file():
            self.counter = self.counter + 1

            return StringIO(f"large file content {self.counter}")

        result = test_large_file()

        assert result == "large file content 1"

        assert cache.exists(cache_key)
        assert cache.load(cache_key) == "large file content 1"

        assert test_large_file() == "large file content 1"

    def test_cached_xml_stream_storage_with_stream_result(self):
        cache = Cache(
            path="/",
            default_ttl=CACHE_UNLIMITED_TTL,
            storage_class=MemoryCacheStorage,
            serializer_class=TextCacheSerializer,
        )

        cache_key = "large-file.txt"

        assert not cache.exists(cache_key)

        self.counter = 0

        @cache.cached(filename=cache_key, stream_result=True)
        def test_large_file():
            self.counter = self.counter + 1

            return StringIO(f"large file content {self.counter}")

        # Memory stream can be not closed
        result = test_large_file()
        assert isinstance(result, StringIO)
        assert result.read() == "large file content 1"

        # Stream can be closed
        with test_large_file() as result:
            assert isinstance(result, StringIO)
            assert result.read() == "large file content 1"

        # Deserialized cache result (not stream)
        assert cache.exists(cache_key)
        assert cache.load(cache_key, stream_result=False) == "large file content 1"

    def test_cached_xml_filesystem_storage_with_stream_result(self):
        cache_dir = tempfile.gettempdir()

        cache = Cache(
            path=cache_dir,
            default_ttl=CACHE_UNLIMITED_TTL,
            storage_class=FileSystemCacheStorage,
            serializer_class=TextCacheSerializer,
        )

        cache_key = "large-file.txt"
        cache.delete(cache_key)

        assert not cache.exists(cache_key)

        self.counter = 0

        @cache.cached(filename=cache_key, stream_result=True)
        def test_large_file():
            self.counter = self.counter + 1

            return BytesIO(f"large file content {self.counter}".encode())

        # Should be closed manually
        with test_large_file() as result:
            assert isinstance(result, BufferedReader)
            assert result.read() == "large file content 1".encode()

        # Should be closed manually
        with test_large_file() as result:
            assert isinstance(result, BufferedReader)
            assert result.read() == "large file content 1".encode()

        # Deserialized cache result (not stream)
        assert cache.exists(cache_key)
        assert cache.load(cache_key, stream_result=False) == "large file content 1"

        cache.delete(cache_key)
