import pendulum
from decimal import Decimal

from weox_import import Product, locales
from weox_import.models import Specification


def test_product_quantity():
    product = Product("1")
    product.set_quantity(1)

    assert product.quantity == 1
    assert product.quantity_str is None


def test_product_quantity_str():
    product = Product("1")

    product.set_quantity(1, "one")
    assert product.quantity == 1
    assert product.quantity_str == "one"


def test_product_quantity_equals_to_quantity_str():
    product = Product("1")

    product.set_quantity(1, "1")
    assert product.quantity == 1
    assert product.quantity_str is None


def test_product_quantity_str_start_with_digit():
    product = Product("1")
    product.set_quantity("5+")
    assert product.quantity == 0
    assert product.quantity_str == "5+"

    product = Product("1")
    product.set_quantity("5+", "Test")
    assert product.quantity == 0
    assert product.quantity_str == "Test"

    product = Product("1")
    product.set_quantity("5+", "5+")
    assert product.quantity == 0
    assert product.quantity_str == "5+"


def test_product_quantity_decimal():
    product = Product("1")
    product.set_quantity("5.0")
    assert product.quantity == 5
    assert product.quantity_str == "5.0"

    product = Product("1")
    product.set_quantity(Decimal("5.0"))
    assert product.quantity == 5
    assert product.quantity_str == "5.0"

    product = Product("1")
    product.set_quantity(5.0)
    assert product.quantity == 5
    assert product.quantity_str == "5.0"


def test_stock_date_in_future():
    product = Product("1")

    utc = pendulum.now("UTC")
    tomorrow = utc.add(days=1)

    product.set_stock_date(tomorrow.isoformat())

    assert product.stock_date == tomorrow.date().isoformat()


def test_stock_date_in_past():
    product = Product("1")

    utc = pendulum.now("UTC")
    yesterday = utc.subtract(days=1)

    product.set_stock_date(yesterday.isoformat())

    assert not product.stock_date


def test_product_ean():
    product = Product("1")

    # Invalid EANs
    product.add_ean(None)
    product.add_ean("")
    product.add_ean("0")
    product.add_ean(0)

    assert len(product.eans) == 0

    # Valid EANs
    product.add_ean("8595606402416")

    assert len(product.eans) == 1


def test_product_ean_prefix():
    product = Product("1")

    # Valid EANs
    product.add_ean("BESTBEFORE-8595606402416")
    product.add_ean("bestbefore-4260208741440")
    product.add_ean("best-before-0075678164125")
    product.add_ean("test-4260208741440")  # invalid prefix will be removed

    assert len(product.eans) == 4
    assert product.eans == [
        "BESTBEFORE-8595606402416",
        "BESTBEFORE-4260208741440",
        "BEST-BEFORE-0075678164125",
        "4260208741440",
    ]


def test_warranty():
    product = Product("1")
    product.set_warranty(12, "12")
    assert product.warranty_months == 12
    assert product.warranty_str is None

    product = Product("1")
    product.set_warranty("12+")
    assert product.warranty_months is None
    assert product.warranty_str == "12+"

    product = Product("1")
    product.set_warranty(24)
    assert product.warranty_months == 24
    assert product.warranty_str is None


def test_sanitize_description():
    product = Product("1")
    product.add_description('<div class="test">test</div>')
    product.add_description("<b>test</b>")

    assert product.descriptions == [
        ("test", None),
        ("<strong>test</strong>", None),
    ]


def test_minimal_product_to_json():
    product = Product("1")

    assert product.to_json() == {
        "id": "1",
        "images": [],
        "eans": [],
        "specifications": [],
        "names": [],
        "descriptions": [],
    }


def test_full_product_to_json():
    product = Product("1")
    product.add_image("http://www.example.com/image1.jpg")
    product.add_ean("190198048387")
    product.add_name("Name", locales.LOCALE_EN)
    product.add_name("Имя", locales.LOCALE_RU)
    product.add_description("Description", locales.LOCALE_EN)
    product.add_description("Описание", locales.LOCALE_RU)

    spec = Specification()
    spec.add_name("Name", locales.LOCALE_EN)
    spec.add_value("Value", locales.LOCALE_EN)
    product.add_specification(spec)

    assert product.to_json() == {
        "id": "1",
        "images": ["http://www.example.com/image1.jpg"],
        "eans": ["190198048387"],
        "specifications": [
            {
                "names": [
                    {
                        "locale": locales.LOCALE_EN,
                        "name": "Name",
                    }
                ],
                "values": [
                    {
                        "locale": locales.LOCALE_EN,
                        "value": "Value",
                        "unit": None,
                    }
                ],
            }
        ],
        "names": [
            {
                "name": "Name",
                "locale": locales.LOCALE_EN,
            },
            {
                "locale": locales.LOCALE_RU,
                "name": "Имя",
            },
        ],
        "descriptions": [
            {
                "description": "Description",
                "locale": locales.LOCALE_EN,
            },
            {
                "description": "Описание",
                "locale": locales.LOCALE_RU,
            },
        ],
    }
