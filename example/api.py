from example.parser import ExampleWeoxParser
from weox_import.api import make_api

parser = ExampleWeoxParser(
    supplier_name="testparser",
    parser_version="20200222",
)

api = make_api(parser)
