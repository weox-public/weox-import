FROM python:3.12.2-bullseye

WORKDIR /build

ENV LANG=C.UTF-8 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    POETRY_VERSION=1.8.2

# Ensures that the python and pip executables used
# in the image will be those from our virtualenv.
ENV PATH="/root/.local/bin:$PATH"

RUN apt-get update && \
    apt-get install --no-install-recommends --no-install-suggests -y build-essential && \
    rm -rf /var/lib/apt/lists/* && \
    curl -sSL https://install.python-poetry.org | POETRY_VERSION=$POETRY_VERSION python3 -

COPY pyproject.toml poetry.lock README.md ./

RUN poetry --version && \
    poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

COPY . .

ARG VERSION

ENV PYTHONPATH=/build

RUN flake8 && \
    pytest tests/ && \
    poetry version $VERSION && \
    poetry build
