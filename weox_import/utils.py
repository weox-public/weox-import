from typing import Optional, Union

import re
from _decimal import Decimal
from gtin.validator import is_valid_GTIN
from lxml import etree
from lxml.etree import CDATA

from weox_import.settings import VALID_EAN_PREFIXES


def add_text_node(el, name: str, value, attrs: dict = None, required: bool = True):
    if not required and is_empty_string(value):
        return

    el = etree.SubElement(el, name)

    if value is None:
        pass
    elif isinstance(value, str):
        el.text = value.strip()
    elif isinstance(value, CDATA):
        el.text = value
    else:
        el.text = str(value).strip()

    if attrs:
        for attr_name, attr_value in attrs.items():
            if not is_empty_string(attr_value):
                el.set(attr_name, str(attr_value).strip())

    return el


def is_valid_ean_prefix(ean_prefix: str) -> bool:
    return str(ean_prefix).upper() in VALID_EAN_PREFIXES


def check_ean(ean: Union[str, int]) -> Optional[str]:
    if not ean:
        return None

    prefix = None

    if not is_numeric(ean):
        m = re.match(r"^([a-zA-Z-]+)-(\d+)$", ean)
        if m:
            prefix, ean = m.groups()

            if not is_valid_ean_prefix(prefix):
                prefix = None

        ean = re.sub(r"[^\d]+", "", ean)

    if is_valid_GTIN(ean):
        return ean if prefix is None else "-".join((prefix.upper(), ean))
    else:
        return None


def is_empty_string(value) -> bool:
    if value is None or value is False:
        return True

    if not isinstance(value, str):
        value = str(value)

    value = value.strip()

    return value == ""


def is_numeric(value) -> bool:
    if value is None or isinstance(value, bool):
        return False

    return str(value).isdigit()


def is_decimal(value) -> bool:
    if value is None or isinstance(value, bool):
        return False

    if is_numeric(value):
        return True

    return bool(re.search(r"^\d+\.\d+$", str(value)))


def is_starts_with_digit(value) -> bool:
    if value is None or isinstance(value, bool):
        return False

    return bool(re.search(r"^\d", str(value)))


def str_to_int(value) -> int:
    if not is_starts_with_digit(value):
        return 0

    return int(re.search(r"^\d+", str(value)).group())


def compose_url(host: str, path: str, port: Optional[int] = None) -> str:
    if port and port == 433:
        protocol = "https"
    else:
        protocol = "http"

    if port and port not in [80, 433]:
        hostname = f"{host}:{port}"
    else:
        hostname = host

    if path != "" and path != "/":
        path = f"/{path.lstrip('/')}"

    return f"{protocol}://{hostname}{path}"


def fix_price(price: Union[Decimal, str, float, int, None]) -> Decimal:
    if price is None:
        raise ValueError("Empty price")
    elif isinstance(price, Decimal):
        return price
    elif isinstance(price, float) or isinstance(price, int):
        return Decimal(str(price))
    else:
        str_price = str(price)
        str_price = str_price.replace(",", ".").replace(" ", "").strip()

        if str_price.replace(".", "", 1).isdigit():
            return Decimal(str_price)
        else:
            raise ValueError(f"Not a valid number: {price}")
