from html_sanitizer import Sanitizer

sanitizer = Sanitizer()


def sanitize(html: str) -> str:
    if html is None or html == "":
        return ""
    elif not isinstance(html, str):
        html = str(html)

    if "<" not in html:
        return html

    return sanitizer.sanitize(html)
