from typing import Iterable, Optional, Union

import datetime
import hashlib
import os
from loguru import logger
from lxml import etree
from pathlib import Path

from weox_import import settings
from weox_import.exceptions import InvalidFormatError
from weox_import.models import Product
from weox_import.utils import add_text_node, is_empty_string
from weox_import.validator import Validator
from weox_import.version import __version__ as weox_import_version


class Pricelist(object):
    def __init__(self, supplier_name: str, parser_version: str):

        logger.info(f"Starting {supplier_name} v{parser_version} import.")

        self.timesatmp = datetime.datetime.now(datetime.UTC)
        self.supplier_name = supplier_name

        self.root = etree.Element("weox_import")
        self.meta = etree.SubElement(self.root, "meta")
        self.products = etree.SubElement(self.root, "products")

        self.add_meta("version", weox_import_version)
        self.add_meta("supplier", supplier_name)
        self.add_meta("parser_version", parser_version)
        self.add_meta("time", self.timesatmp.isoformat())

        self.default_currency = self.add_meta("currency", settings.DEFAULT_CURRENCY)
        self.default_vat = self.add_meta("vat", settings.DEFAULT_VAT)
        self.default_locale = self.add_meta("locale", settings.DEFAULT_LOCALE)

        self.generation_time = self.add_meta("gen_time", "")
        self.hash = self.add_meta("hash", "")

        self.duplicate_products_ids = set()
        self.duplicate_eans = set()
        self.duplicate_skus = set()

    def set_default_currency(self, currency: str):
        self.default_currency.text = currency

    def set_default_vat(self, vat: int):
        self.default_vat.text = str(vat)

    def add_meta(self, name: str, value: str):
        return add_text_node(self.meta, name, value)

    def add_product(self, product: Product):
        if product is None:
            return

        if not self.validate_can_add_product(product):
            logger.warning("Skip product without name. ID: %s" % product.id)
            return

        self._check_duplicates(product)

        el = etree.SubElement(self.products, "product")

        add_text_node(el, "id", product.id)
        add_text_node(el, "old_id", product.old_id, required=False)
        add_text_node(
            el=el,
            name="brand",
            value=product.brand if product.brand else settings.DEFAULT_BRAND,
            required=False,
        )
        add_text_node(el, "sku", product.sku, required=False)
        add_text_node(el, "supplier_code", product.supplier_code, required=False)

        inventory = etree.SubElement(el, "inventory")
        if product.price:
            add_text_node(
                el=inventory,
                name="price",
                value=product.price,
                attrs={
                    "c": product.price_currency,
                    "v": product.price_vat,
                },
            )

        if product.retail_price:
            add_text_node(
                el=inventory,
                name="retail_price",
                value=product.retail_price,
                attrs={
                    "c": product.retail_price_currency,
                },
            )

        if product.old_retail_price:
            add_text_node(
                el=inventory,
                name="old_retail_price",
                value=product.old_retail_price,
                attrs={
                    "c": product.old_retail_price_currency,
                },
            )

        add_text_node(inventory, "quantity", product.quantity)
        add_text_node(inventory, "quantity_str", product.quantity_str, required=False)
        add_text_node(inventory, "stock_date", product.stock_date, required=False)
        add_text_node(inventory, "warranty", product.warranty_months, required=False)
        add_text_node(inventory, "warranty_str", product.warranty_str, required=False)

        if product.eans:
            eans = etree.SubElement(el, "eans")
            for ean in product.eans:
                add_text_node(eans, "e", ean)

        categories = etree.SubElement(el, "categories")
        product_categories = product.categories or [settings.DEFAULT_CATEGORY]
        for category in product_categories:
            add_text_node(categories, "c", category, required=False)

        names = etree.SubElement(el, "names")
        for name, locale in product.names:
            add_text_node(
                el=names,
                name="n",
                value=name,
                attrs={
                    "l": locale,
                },
            )

        if product.descriptions:
            descriptions = etree.SubElement(el, "descriptions")
            for description, locale in product.descriptions:
                add_text_node(
                    el=descriptions,
                    name="d",
                    value=etree.CDATA(description),
                    attrs={
                        "l": locale,
                    },
                )

        if product.images:
            images = etree.SubElement(el, "images")
            for url in product.images:
                add_text_node(images, "i", url)

        if product.specifications:
            specs = []
            for specification in product.specifications:
                if specification.is_valid:
                    specs.append(specification)
                else:
                    logger.warning(
                        "Skip empty specifications element. ID: %s" % product.id
                    )

            if specs:
                specifications = etree.SubElement(el, "specification")

                for specification in specs:
                    s = etree.SubElement(specifications, "s")

                    for name, locale in specification.names:
                        add_text_node(
                            el=s,
                            name="n",
                            value=name,
                            attrs={
                                "l": locale,
                            },
                        )

                    for value, locale, unit in specification.values:
                        add_text_node(
                            el=s,
                            name="v",
                            value=value,
                            attrs={
                                "l": locale,
                                "u": unit,
                            },
                        )

        add_text_node(
            el=el,
            name="weox_product_info_url",
            value=product.weox_product_info_url,
            required=False,
        )

        add_text_node(
            el=el,
            name="hash",
            value=product.hash,
            required=False,
        )

        self._recalculate_hash(product.hash)

    def add_products(self, products: Iterable[Product]):
        for product in products:
            self.add_product(product)

    def to_xml(self, pretty_print: bool = False) -> bytes:
        self._pre_save()

        return etree.tostring(self.root, encoding="utf-8", pretty_print=pretty_print)

    def validate(self, xsd_path: str = None):
        self._pre_save()

        validator = Validator(xsd_path)
        is_valid = validator.validate(self.root)

        return (
            is_valid,
            None if is_valid else validator.last_error(),
        )

    def to_pretty_xml(self) -> str:
        return self.to_xml(pretty_print=True).decode("utf-8")

    def save(self, path: Union[str, Path], pretty_print: bool = False):
        self._pre_save()

        et = etree.ElementTree(self.root)
        with open(path, "wb") as fp:
            et.write(
                fp,
                xml_declaration=True,
                encoding="utf-8",
                pretty_print=pretty_print,
            )

    def publish(
        self,
        publish_dir: Optional[Union[str, Path]] = None,
        publish_file_name: Optional[str] = None,
        pretty_print: Optional[bool] = False,
    ) -> Path:

        valid, error = self.validate()

        if not publish_dir:
            publish_dir = os.environ.get("PUBLISH_DIR", settings.DEFAULT_PUBLISH_DIR)

        if not publish_file_name:
            timestamp = int(datetime.datetime.now(datetime.UTC).timestamp())
            publish_file_name = f"{timestamp}_{self.supplier_name}.xml"

        xml_path = Path(publish_dir).resolve().joinpath(publish_file_name)

        if valid:
            logger.info(f"Saving xml to {xml_path}")
            self.save(xml_path, pretty_print)

            try:
                file_size_bytes = xml_path.stat().st_size
            except Exception:
                logger.exception(f"Unable to get filesize {xml_path}")
                file_size_bytes = 0

            def human_readable_size(size, decimal_places=2):
                for unit in ["B", "KiB", "MiB", "GiB", "TiB", "PiB"]:
                    if size < 1024.0 or unit == "PiB":
                        break
                    size /= 1024.0
                return f"{size:.{decimal_places}f} {unit}"

            logger.info(f"Products count: {len(self.products)}")
            logger.info(f"Done. File size: {human_readable_size(file_size_bytes)}")

            return xml_path
        else:
            logger.error(f"XSD Validation error: {error}")

            raise InvalidFormatError(error)

    def _pre_save(self):
        self.generation_time.text = str(self._get_generation_time())

    def _get_generation_time(self) -> int:
        return (datetime.datetime.now(datetime.UTC) - self.timesatmp).seconds

    def _recalculate_hash(self, product_hash: str):
        payload = self.hash.text + product_hash

        self.hash.text = hashlib.md5(payload.encode("utf-8")).hexdigest()

    def validate_can_add_product(self, product: Product):
        if not isinstance(product, Product):
            return False

        if not product.id:
            return False

        if not product.names:
            return False

        return True

    def _check_duplicates(self, product: Product):
        if product.id in self.duplicate_products_ids:
            logger.warning("Duplicate product ID: %s" % product.id)
        else:
            self.duplicate_products_ids.add(product.id)

        for ean in product.eans:
            if ean in self.duplicate_eans:
                logger.debug("Duplicate EAN %s Product ID: %s" % (ean, product.id))
            else:
                self.duplicate_eans.add(ean)

        if not is_empty_string(product.sku):
            if product.sku in self.duplicate_skus:
                logger.debug(
                    "Duplicate SKU %s Product ID: %s" % (product.sku, product.id)
                )
            else:
                self.duplicate_skus.add(product.sku)
