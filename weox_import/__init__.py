import weox_import.locales  # noqa: F401
from weox_import.models import Product, Specification  # noqa: F401
from weox_import.pricelist import Pricelist  # noqa: F401
