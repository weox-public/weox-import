from typing import Generator, Optional, Union

from abc import ABC, abstractmethod
from loguru import logger
from pathlib import Path
from starlette.responses import Response

from weox_import import Pricelist, Product
from weox_import.exceptions import NotImplementedException


class WeoxParser(ABC):
    def __init__(self, supplier_name: str, parser_version: str):
        self.supplier_name = supplier_name
        self.parser_version = parser_version
        self.logger = logger

    def parse(self, publish_dir: Optional[Union[str, Path]] = None) -> Path:
        """
        Parse pricelist and save result to the publishing directory.
        Production version.

        :param publish_dir: Optional publish directory
        :return: Full path to the published file
        """
        self.logger.info(f"{self.supplier_name} parser started in production mode")

        pricelist = Pricelist(self.supplier_name, self.parser_version)
        pricelist.add_products(self.parse_pricelist())
        return pricelist.publish(
            publish_dir=publish_dir,
        )

    def parse_development(
        self, publish_dir: Union[str, Path], publish_file_name: Optional[str] = None
    ) -> Path:
        """
        Parse pricelist and save result to the publishing directory.
        Development preview version.

        :param publish_dir: Optional publish directory
        :return: Full path to the published file
        """
        self.logger.info(f"{self.supplier_name} parser started in development mode")

        pricelist = Pricelist(self.supplier_name, self.parser_version)
        pricelist.add_products(self.parse_pricelist())

        if not publish_file_name:
            publish_file_name = f"{self.supplier_name}.xml"

        return pricelist.publish(
            publish_dir=publish_dir,
            publish_file_name=publish_file_name,
            pretty_print=True,
        )

    @abstractmethod
    def parse_pricelist(self) -> Generator[Product, None, None]:
        raise NotImplementedException()

    def download_picture(self, picture_id: str) -> Response:
        raise NotImplementedException()

    def download_product_info(self, product_id: str) -> Product:
        raise NotImplementedException()
