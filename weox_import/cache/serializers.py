from typing import Any, Iterator, Optional, Union

import pickle
import simplejson as simplejson
from abc import ABC, abstractmethod
from io import IOBase, StringIO
from loguru import logger
from pathlib import Path


class CacheSerializer(ABC):
    def __init__(self, binary: bool = False):
        self.binary = binary

    @abstractmethod
    def serialize(self, data, stream=None):
        pass

    @abstractmethod
    def unserialize(self, data=None, stream=None):
        pass

    def unserialize_stream(self, stream=None):
        return self.unserialize(stream=stream)

    def destream(self, stream) -> Any:
        # TODO: remove? not used anymore!

        if (
            isinstance(stream, dict)
            or isinstance(stream, str)
            or isinstance(stream, list)
        ):
            return stream

        if isinstance(stream, bytes):
            return stream.decode()

        if isinstance(stream, IOBase):
            if stream.seekable() and stream.tell() > 0:
                stream.seek(0)

            return stream.read()  # TODO: may raise Error

        if isinstance(stream, Iterator):
            result = StringIO()

            for chunk in stream:
                result.write(chunk)

            result.seek(0)
            return result.read()

        raise RuntimeError(f"Unsupported stream type {type(stream)}")

    def format_file_path(self, file_path: Union[str, Path]) -> str:
        return str(file_path)


class JsonCacheSerializer(CacheSerializer):
    def __init__(self):
        super().__init__(binary=False)

    def serialize(self, data: str, stream=None):
        if stream is not None:
            simplejson.dump(data, fp=stream, indent=" ", encoding="utf-8")
        elif data is not None:
            return simplejson.dumps(data, indent=" ", encoding="utf-8")
        else:
            logger.warning("JsonCacheSerializer empty data to serialize")

    def unserialize(self, data=None, stream=None) -> Optional[any]:
        if stream is not None:
            if isinstance(stream, IOBase) and stream.seekable() and stream.tell() > 0:
                stream.seek(0)

            return simplejson.load(stream, encoding="utf-8")
        elif data is not None:
            return simplejson.loads(data, encoding="utf-8")
        else:
            logger.warning("JsonCacheSerializer empty data to unserialize")
            return None

    def format_file_path(self, key: str) -> str:
        if not str(key).lower().endswith(".json"):
            return f"{key}.json"
        else:
            return key


class PickleCacheSerializer(CacheSerializer):
    def __init__(self):
        super().__init__(binary=True)

    def serialize(self, data: str, stream=None):
        if stream is not None:
            pickle.dump(data, stream, protocol=pickle.HIGHEST_PROTOCOL)
        elif data is not None:
            return pickle.dumps(data, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            logger.warning("PickleCacheSerializer empty data to serialize")

    def unserialize(self, data=None, stream=None) -> Optional[any]:
        if stream is not None:
            return pickle.load(stream)
        elif data is not None:
            return pickle.loads(data)
        else:
            logger.warning("PickleCacheSerializer empty data to unserialize")
            return None

    def format_file_path(self, key: str) -> str:
        if not key.lower().endswith(".pickle"):
            return f"{key}.pickle"
        else:
            return key


class TextCacheSerializer(CacheSerializer):
    def __init__(self):
        super().__init__(binary=False)

    def serialize(self, data: str, stream=None):
        if stream is not None:
            if isinstance(data, Iterator):
                for chunk in data:
                    stream.write(chunk)
            else:
                stream.write(data)
        elif data is not None:
            return data
        else:
            logger.warning("TextCacheSerializer empty data to serialize")

    def unserialize(self, data=None, stream=None) -> Optional[any]:
        if stream is not None:
            if isinstance(stream, IOBase) and stream.seekable() and stream.tell() > 0:
                stream.seek(0)

            return stream.read()
        elif data is not None:
            return data
        else:
            logger.warning("TextCacheSerializer empty data to unserialize")
            return None

    def unserialize_stream(self, stream=None):
        if isinstance(stream, IOBase) and stream.seekable() and stream.tell() > 0:
            stream.seek(0)

        return stream
