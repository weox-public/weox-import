from typing import Optional, Union

from loguru import logger
from pathlib import Path

from weox_import.cache import constants, serializers, storage


class Cache(object):
    def __init__(
        self,
        path: Union[str, Path],
        default_ttl: int = constants.CACHE_DISABLED_TTL,
        storage_class=None,
        serializer_class=None,
    ):
        self.path = Path(path)
        self.default_ttl = default_ttl
        self.unlimited_ttl = False

        if not storage_class:
            storage_class = storage.FileSystemCacheStorage

        if not serializer_class:
            serializer_class = serializers.TextCacheSerializer

        self.storage: "storage.CacheStorage" = storage_class(serializer_class())

    def make_cache_path(self, filename: str) -> Path:
        return self.path.joinpath(filename)

    def make_cache_serializer_path(self, filename: str) -> str:
        cache_path = self.make_cache_path(filename)
        return self.storage.serializer.format_file_path(cache_path)

    def set_unlimited_ttl(self, value: bool = True):
        self.unlimited_ttl = value

    def get_cache_ttl(self, ttl: Optional[int]) -> int:
        if self.unlimited_ttl:
            return constants.CACHE_UNLIMITED_TTL
        else:
            return self.default_ttl if ttl is None else ttl

    def cached_json_serializable(self, filename: str, ttl: int = None, **kwargs):
        return self.cached(
            filename=filename,
            ttl=ttl,
            serializer_class=serializers.JsonCacheSerializer,
            binary=False,
            **kwargs,
        )

    def cached_pickle_serializable(self, filename: str, ttl: int = None, **kwargs):
        return self.cached(
            filename=filename,
            ttl=ttl,
            serializer_class=serializers.PickleCacheSerializer,
            **kwargs,
        )

    def cached(self, filename: str, ttl: int = None, **kwargs):
        """
        Cache any text or stream. Example: raw xml

        :param filename: string
        :param ttl: int
        :param serializer_class: str serializer class
        :param stream_result: bool if False - result stream decoding
        :param serializer: str deprecated. use serializer_class
        """

        if "serializer" in kwargs:
            serializer_class = kwargs["serializer"]
        else:
            serializer_class = kwargs.get("serializer_class", None)

        stream_result = kwargs.get("stream_result", False)
        binary = kwargs.get("binary", None)

        if stream_result and binary is None:
            binary = True

        def cached_decorator(func):
            def wrapped(*wrapped_args, **wrapped_kwargs):
                cache_ttl = self.get_cache_ttl(ttl)
                filename_formatted = filename.format(*wrapped_args)
                orig_serializer = None

                if (
                    serializer_class
                    and self.storage.serializer.__class__ != serializer_class
                ):
                    orig_serializer = self.storage.serializer
                    self.storage.serializer = serializer_class()

                if binary:
                    self.storage.serializer.binary = True

                result = self.load(
                    filename=filename_formatted,
                    ttl=cache_ttl,
                    stream_result=stream_result,
                )

                if result is None:
                    result = func(*wrapped_args, **wrapped_kwargs)
                    if result is not None:
                        self.save(
                            filename=filename_formatted,
                            result=result,
                            ttl=cache_ttl,
                        )

                        # TODO: check cache_ttl (unlimited ttl is hack)
                        result = self.load(
                            filename=filename_formatted,
                            ttl=constants.CACHE_UNLIMITED_TTL,
                            stream_result=stream_result,
                        )

                if orig_serializer:
                    self.storage.serializer = orig_serializer

                return result

            return wrapped

        return cached_decorator

    def load(
        self,
        filename: str,
        ttl: Optional[int] = None,
        stream_result: bool = False,
    ) -> Optional[str]:
        if ttl is None:
            ttl = self.default_ttl

        if ttl == constants.CACHE_DISABLED_TTL:
            logger.info(f"Cache disabled for {filename}")
            self.delete(filename)

            return

        cache_path = self.make_cache_serializer_path(filename)
        logger.info(f"Loading {filename} from cache {cache_path}")
        result = self.storage.load(cache_path, ttl, stream_result)

        if not result:
            result = None
            logger.info(f"Not found {filename} in cache")

        return result

    def save(self, filename: str, result: str, ttl: int):
        if ttl == constants.CACHE_DISABLED_TTL:
            logger.info(
                f"Cache disabled for {filename}, but saving to load on the next step"
            )
            ttl = constants.CACHE_UNLIMITED_TTL

        cache_path = self.make_cache_serializer_path(filename)
        logger.info(f"Saving {filename} cache {cache_path}")
        self.storage.save(
            cache_path=cache_path,
            result=result,
            ttl=ttl,
        )

    def exists(self, filename: str) -> bool:
        return self.storage.exists(self.make_cache_serializer_path(filename))

    def delete(self, filename: str) -> bool:
        if self.exists(filename):  # Not safe
            return self.storage.delete(self.make_cache_serializer_path(filename))
        else:
            return False
