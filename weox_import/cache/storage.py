from typing import Optional

import os
import time
from abc import ABC, abstractmethod
from copy import deepcopy
from io import StringIO

from weox_import.cache.constants import CACHE_UNLIMITED_TTL
from weox_import.cache.serializers import CacheSerializer


class CacheStorage(ABC):
    def __init__(self, serializer: "CacheSerializer"):
        self.serializer = serializer

    @abstractmethod
    def save(self, cache_path: str, result, ttl: int):
        pass

    @abstractmethod
    def load(self, cache_path: str, ttl: int, stream_result: bool) -> Optional[str]:
        pass

    @abstractmethod
    def exists(self, cache_path: str) -> bool:
        pass

    @abstractmethod
    def delete(self, cache_path: str) -> bool:
        pass

    @abstractmethod
    def is_expired(self, cache_path: str, ttl: int) -> bool:
        pass


class FileSystemCacheStorage(CacheStorage):
    def save(self, cache_path: str, result: any, ttl: int):
        if self.exists(cache_path):
            self.delete(cache_path)  # Not safe

        if isinstance(result, StringIO):
            self.serializer.binary = False

        with open(cache_path, self.write_mode, encoding=self.encoding) as fp:
            self.serializer.serialize(result, stream=fp)

    def load(self, cache_path: str, ttl: int, stream_result: bool) -> Optional[any]:
        if self.is_expired(cache_path, ttl):
            if self.exists(cache_path):
                self.delete(cache_path)

            return None

        if stream_result:
            self.serializer.binary = True
        else:
            self.serializer.binary = False

        if stream_result and self.serializer.binary:
            fp = open(cache_path, mode=self.read_mode, encoding=self.encoding)
            return self.serializer.unserialize_stream(stream=fp)
        else:
            with open(cache_path, mode=self.read_mode, encoding=self.encoding) as fp:
                return self.serializer.unserialize(stream=fp)

    def is_expired(self, cache_path: str, ttl: int) -> bool:
        if not self.exists(cache_path):
            return True
        elif ttl == CACHE_UNLIMITED_TTL:
            return False

        modification_time = int(os.path.getmtime(cache_path))
        current_time = int(time.time())
        secs_elasped = current_time - modification_time

        return secs_elasped > ttl

    def exists(self, key: str) -> bool:
        return os.path.isfile(key)

    def delete(self, key: str) -> bool:
        os.remove(key)

        return True

    @property
    def write_mode(self):
        if self.serializer.binary:
            return "wb"
        else:
            return "w"

    @property
    def read_mode(self):
        if self.serializer.binary:
            return "rb"
        else:
            return "r"

    @property
    def encoding(self):
        if self.serializer.binary:
            return None
        else:
            return "utf-8"


class MemoryCacheStorage(CacheStorage):
    def __init__(self, serializer: "CacheSerializer"):
        super().__init__(serializer)

        self.cache = dict()

    def save(self, cache_path: str, result: any, ttl: int):
        cached = StringIO()

        self.serializer.serialize(data=result, stream=cached)

        self.cache[cache_path] = cached

    def load(self, cache_path: str, ttl: int, stream_result: bool) -> Optional[any]:
        result = self.cache.get(cache_path)

        if result is None:
            return
        elif stream_result:
            # Stream can be closed and cannot be used anymore
            # memory cache is only used for testing
            return self.serializer.unserialize_stream(stream=deepcopy(result))
        else:
            return self.serializer.unserialize(stream=result)

    def exists(self, cache_path: str) -> bool:
        return cache_path in self.cache

    def delete(self, cache_path: str) -> bool:
        del self.cache[cache_path]

        return True

    def is_expired(self, cache_path: str, ttl: int) -> bool:
        if not self.exists(cache_path):
            return True
        elif ttl == CACHE_UNLIMITED_TTL:
            return False

        return False
