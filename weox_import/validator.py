import os
from lxml import etree


class Validator(object):
    def __init__(self, xsd_path: str = None):
        if not xsd_path:
            current_dir = os.path.dirname(os.path.abspath(__file__))
            xsd_path = os.path.join(current_dir, "xml.xsd")

        xmlschema_doc = etree.parse(xsd_path)
        self.xmlschema = etree.XMLSchema(xmlschema_doc)

    def validate_file(self, xml_path: str):
        xml_doc = etree.parse(xml_path)

        return self.validate(xml_doc)

    def validate(self, xml_doc):
        return self.xmlschema.validate(xml_doc)

    def last_error(self):
        return self.xmlschema.error_log
